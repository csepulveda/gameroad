class LocationKind:
    WEBSITE = 'WEBSITE'
    MOBILE = 'MOBILE'

    CHOICES = (
        (WEBSITE, WEBSITE),
        (MOBILE, MOBILE),
    )
