# django
from django.db import models
from django.utils.translation import ugettext_lazy as _

# geodjango
from django.contrib.gis.db.models import PointField
from django.contrib.gis.geos import Point

# base model
from base.models import BaseGisModel

# enums
from logs.enums import LocationKind


class Location(BaseGisModel):
    address = models.CharField(
        _('address'),
        max_length=255,
    )
    location = PointField(
        default=Point(-70.5059999227523804, -33.4040476351846252),
    )
    kind = models.CharField(
        _('kind'),
        max_length=50,
        choices=LocationKind.CHOICES,
    )

    def __str__(self):
        return self.address
