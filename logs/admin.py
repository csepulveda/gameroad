# django
from django.contrib import admin
from django.contrib.gis.db.models import PointField

# models
from logs.models.locations import Location

# mapswidget
from mapwidgets.widgets import GooglePointFieldWidget


@admin.register(Location)
class LocationAdmin(admin.ModelAdmin):
    date_hierarchy = 'created_at'
    list_filter = (
        'kind',
    )
    list_display = (
        'address',
        'kind',
        'created_at',
    )

    formfield_overrides = {
        PointField: {'widget': GooglePointFieldWidget}
    }
