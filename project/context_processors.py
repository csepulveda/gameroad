from django.conf import settings


def google_analytics_code(request):
    return {
        "google_analytics_code": settings.GOOGLE_ANALYTICS_CODE,
        "google_map_api_key": settings.GOOGLE_MAP_API_KEY_FRONTEND,
    }
