const GeoFindMe = () => {
  const inputLocation = window.document.getElementById('location');

  if (!window.navigator.geolocation) {
    return;
  }
  if (!inputLocation || inputLocation.value) {
    return;
  }

  const getAddress = (latitude, longitude) => {
    const latlng = { lat: parseFloat(latitude), lng: parseFloat(longitude) };
    const geocoder = new window.google.maps.Geocoder();

    geocoder.geocode({ location: latlng }, (results) => {
      inputLocation.value = results[0].formatted_address;
    });
  };

  const success = (position) => {
    const { latitude, longitude } = position.coords;
    getAddress(latitude, longitude);
  };

  const error = () => {
  };

  window.navigator.geolocation.getCurrentPosition(success, error);
};

export default GeoFindMe;
