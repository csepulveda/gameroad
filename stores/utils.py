# models
from logs.models import Location

# services
from stores import services


def get_current_location(location: str, queryset, location_kind: str):
    current_location = services.get_geo_point_from_address(location)

    if not current_location:
        return [None, queryset.none()]

    Location.objects.create(
        address=location,
        location=current_location,
        kind=location_kind,
    )

    return [
        current_location,
        queryset.search_by_location(
            current_location=current_location,
            meters=20000,
        ),
    ]
