# -*- coding: utf-8 -*-
""" Views for the stores application. """
# standard library

# django
from django.urls import reverse
from django.contrib import messages

# models
from stores.models import Store
from stores.models import DraftStore
from stores.models import Address

# views
from base.views import BaseDetailView
from base.views import BaseListView
from base.views import CreateView

# utils
from stores import utils

# forms
from stores.forms import DraftStoreForm

# enums
from logs.enums import LocationKind


class AddressSearchListView(BaseListView):
    """
    View for displaying a list of stores.
    """

    model = Address
    template_name = "address/address_list.pug"
    login_required = False
    paginate_by = 15

    def setup(self, request, *args, **kwargs):
        super().setup(request, *args, **kwargs)
        self.location = self.request.GET.get("location", None)
        self.current_location = None

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context["current_location"] = self.current_location
        return context

    def get_queryset(self):
        queryset = super().get_queryset()
        queryset = queryset.active().active_stores().related()

        if not self.location:
            return queryset

        self.current_location, queryset = utils.get_current_location(
            self.location,
            queryset,
            LocationKind.WEBSITE,
        )

        return queryset.only_physical_stores()


class StoreDetailView(BaseDetailView):
    model = Store
    template_name = "stores/store_detail.pug"
    login_required = False
    query_pk_and_slug = True

    def get_queryset(self):
        queryset = super().get_queryset()
        return queryset.related().active()


class DraftStoreCreateView(CreateView):
    model = DraftStore
    form_class = DraftStoreForm
    template_name = "stores/store_create.pug"
    login_required = False

    def get_success_url(self):
        messages.add_message(
            self.request,
            messages.SUCCESS,
            "¡Muchas gracias! Pronto podrás ver la tienda en el buscador",
        )
        return reverse("draft_store_create")


class StoreOnlineListView(BaseListView):
    model = Store
    login_required = False
    template_name = "stores/store_list.pug"
    paginate_by = 50

    def get_queryset(self):
        queryset = super().get_queryset()
        queryset = queryset.only_online_stores().active()
        return queryset
