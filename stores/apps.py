from django.apps import AppConfig
from django.utils.translation import ugettext_lazy as _


class StoresConfig(AppConfig):
    name = 'stores'
    verbose_name = _('stores')

    def ready(self):
        from .models.images import Image
        from .models.schedules import Schedule
        from .models.addresses import Address
