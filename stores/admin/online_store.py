# django
from django.contrib import admin

# models
from stores.models import OnlineStore

from stores.forms import OnlineStoreForm

from .stores import StoreAdminMixin


@admin.register(OnlineStore)
class OnlineStoreAdmin(StoreAdminMixin):
    form = OnlineStoreForm

    def get_queryset(self, request):
        queryset = super().get_queryset(request)
        queryset = queryset.only_online_stores()
        return queryset
