# django
from django.contrib import admin

# models
from stores.models import PhysicalStore

from .stores import StoreAdminMixin

# inlines
from .schedules import ScheduleInline
from .images import ImageInline
from .addresses import AddressInline


@admin.register(PhysicalStore)
class PhysicalStoreAdmin(StoreAdminMixin):
    inlines = (
        AddressInline,
        ScheduleInline,
        ImageInline,
    )

    def get_queryset(self, request):
        queryset = super().get_queryset(request)
        queryset = queryset.only_physical_stores()
        return queryset
