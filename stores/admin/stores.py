# django
from django.contrib import admin

# models
from stores.models import Store

# inlines
from .schedules import ScheduleInline
from .images import ImageInline
from .addresses import AddressInline


class StoreAdminMixin(admin.ModelAdmin):
    list_display = (
        'name',
        'website',
        'facebook',
        'instagram',
        'is_online_store',
        'is_active',
    )
    search_fields = (
        'name',
    )
    list_filter = (
        'is_active',
        'is_online_store',
    )


@admin.register(Store)
class StoreAdmin(StoreAdminMixin):
    inlines = (
        AddressInline,
        ScheduleInline,
        ImageInline,
    )
