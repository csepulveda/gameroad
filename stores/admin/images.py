# django
from django.contrib import admin
from django.utils.safestring import mark_safe

# models
from stores.models.images import Image


class ImageInline(admin.TabularInline):
    model = Image
    extra = 1


@admin.register(Image)
class ImageAdmin(admin.ModelAdmin):
    list_display = (
        'store',
        'get_image',
        'is_active',
        'display_order',
    )
    search_fields = (
        'store__name',
    )
    list_filter = (
        'is_active',
        'store__name',
    )

    def get_image(self, instance):
        return mark_safe(
            f'<img src=\"{instance.image["admin_list"].url}\"/>'
        )
    get_image.short_description = 'image'
