from django.contrib import admin

from stores.models import DraftStore


@admin.register(DraftStore)
class DraftStoreAdmin(admin.ModelAdmin):
    date_hierarchy = 'created_at'
    list_display = (
        'name',
        'website',
        'facebook',
        'instagram',
        'is_online_store',
        'address',
    )
    search_fields = (
        'name',
    )
    list_filter = (
        'is_online_store',
    )
