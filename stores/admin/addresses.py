# django
from django.contrib import admin
from django.contrib.gis.db.models import PointField

# models
from stores.models.addresses import Address

# mapswidget
from mapwidgets.widgets import GooglePointFieldWidget


class AddressInline(admin.StackedInline):
    model = Address
    min_num = 1
    extra = 0

    def get_min_num(self, request, obj=None, **kwargs):
        if obj and obj.is_online_store:
            return 0
        return super().get_min_num(request, obj, **kwargs)

    formfield_overrides = {
        PointField: {'widget': GooglePointFieldWidget}
    }

    def get_extra(self, request, obj=None, **kwargs):
        return self.extra


@admin.register(Address)
class AddressAdmin(admin.ModelAdmin):
    list_display = (
        'store',
        'address',
        'is_active',
    )
    search_fields = (
        'store__name',
    )
    list_filter = (
        'is_active',
        'store__name',
    )
    formfield_overrides = {
        PointField: {'widget': GooglePointFieldWidget}
    }
