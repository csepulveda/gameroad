# django
from django.contrib import admin

# models
from stores.models.schedules import Schedule


class ScheduleInline(admin.TabularInline):
    model = Schedule
    max_num = 7
    extra = 1


@admin.register(Schedule)
class ScheduleAdmin(admin.ModelAdmin):
    list_display = (
        'store',
        'weekday',
        'opening_time',
        'closing_time',
    )
    search_fields = (
        'store__name',
    )
    list_filter = (
        'store__name',
    )
