from stores.models import Store


class OnlineStore(Store):

    class Meta:
        proxy = True
        ordering = (
            'name',
        )

    def save(self, *args, **kwargs):
        self.is_online_store = True
        return super().save(*args, **kwargs)
