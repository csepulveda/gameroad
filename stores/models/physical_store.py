from stores.models import Store


class PhysicalStore(Store):

    class Meta:
        proxy = True
        ordering = (
            'name',
        )

    def save(self, *args, **kwargs):
        self.is_online_store = False
        return super().save(*args, **kwargs)
