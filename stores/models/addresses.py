# django
from django.db import models
from django.utils.translation import ugettext_lazy as _

# geodjango
from django.contrib.gis.db.models import PointField
from django.contrib.gis.geos import Point

# base
from base.models import BaseGisModel

# managers
from stores.managers.addresses import AddressQuerySet


class Address(BaseGisModel):
    store = models.ForeignKey(
        'stores.Store',
        verbose_name=_('store'),
        related_name='addresses',
        on_delete=models.CASCADE,
    )
    address = models.CharField(
        _('address'),
        max_length=255
    )
    location = PointField(
        default=Point(-70.5059999227523804, -33.4040476351846252),
    )
    is_active = models.BooleanField(
        _('is active'),
        default=True,
    )

    objects = AddressQuerySet.as_manager()

    def __str__(self):
        return self.address

    class Meta:
        verbose_name = _('address')
        verbose_name_plural = _('addresses')
        ordering = ('address',)

    @property
    def store_name(self):
        return self.store.name

    def get_map_url(self):
        lat, long = self.location.coords
        return f'https://www.google.com/maps/dir/?api=1&destination={long},{lat}'
