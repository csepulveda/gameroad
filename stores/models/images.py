# django
from django.db import models
from django.utils.translation import ugettext_lazy as _

# base
from base.models import OrderableModel
from base.models import file_path

# easy thumbnail
from easy_thumbnails.fields import ThumbnailerImageField

# managers
from stores.managers.images import ImageQuerySet


class Image(OrderableModel):
    store = models.ForeignKey(
        'stores.Store',
        verbose_name=_('store'),
        related_name='images',
        on_delete=models.CASCADE
    )
    image = ThumbnailerImageField(
        _('image'),
        upload_to=file_path,
    )
    is_active = models.BooleanField(
        _('is active'),
        default=True,
    )

    objects = ImageQuerySet.as_manager()
