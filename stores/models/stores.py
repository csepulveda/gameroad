# django
from django.db import models
from django.urls import reverse
from django.utils.translation import ugettext_lazy as _
from django.utils.text import slugify
from django.core.exceptions import ValidationError

# base
from base.models import BaseModel

# managers
from stores.managers.stores import StoreQuerySet


class BaseStore(BaseModel):
    name = models.CharField(
        _('name'),
        max_length=255,
    )
    website = models.URLField(
        _('website'),
        max_length=255,
        blank=True,
    )
    facebook = models.URLField(
        _('facebook'),
        max_length=150,
        blank=True,
    )
    instagram = models.URLField(
        _('instagram'),
        max_length=150,
        blank=True,
    )
    is_online_store = models.BooleanField(
        _('is online store'),
        default=False
    )

    class Meta:
        abstract = True

    def __str__(self):
        return self.name

    def clean(self):
        if self.is_online_store and not self.website:
            return ValidationError(
                {
                    'website': _('this field can\'t be empty.')
                }
            )


class Store(BaseStore):
    # required fields
    is_active = models.BooleanField(
        _('is active'),
        default=True,
    )
    slug = models.SlugField(
        blank=True,
        null=True,
        editable=False,
    )

    # Manager
    objects = StoreQuerySet.as_manager()

    class Meta:
        verbose_name = _('store')
        verbose_name_plural = _('stores')
        ordering = ('name',)

    def save(self, *args, **kwargs):
        self.name = self.name.strip()
        if not self.slug:
            self.slug = slugify(self.name)
        super().save(*args, **kwargs)

    def get_absolute_url(self):
        return reverse(
            'store_detail', kwargs={
                'slug': self.slug,
                'pk': self.pk,
            }
        )
