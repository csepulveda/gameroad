# django
from django.db import models
from django.utils.translation import ugettext_lazy as _

# base
from base.models import BaseModel

# enums
from stores.enums import StoreScheduleWeekday


class Schedule(BaseModel):
    store = models.ForeignKey(
        'stores.Store',
        verbose_name=_('store'),
        related_name='schedules',
        on_delete=models.CASCADE,
    )
    weekday = models.PositiveSmallIntegerField(
        _('weekday'),
        choices=StoreScheduleWeekday.CHOICES,
    )
    opening_time = models.TimeField(
        _('opening time'),
    )
    closing_time = models.TimeField(
        _('closing time'),
    )

    class Meta:
        ordering = (
            'weekday',
        )
        unique_together = (
            ('store', 'weekday'),
        )

    def __str__(self):
        return (
            f'{self.get_weekday_display()} '
            f'| {self.opening_time} - {self.closing_time}'
        )
