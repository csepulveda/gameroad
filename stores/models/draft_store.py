# django
from django.db import models
from django.utils.translation import ugettext_lazy as _

from stores.models.stores import BaseStore


class DraftStore(BaseStore):
    address = models.CharField(
        _('address'),
        max_length=255,
        blank=True,
        null=True,
    )
