# base
from base.managers import QuerySet

# geo django
from django.db.models import Prefetch

# models
from stores.models.addresses import Address
from stores.models.images import Image


class StoreQuerySet(QuerySet):

    def active(self):
        return self.filter(
            is_active=True,
        )

    def only_physical_stores(self):
        return self.filter(
            is_online_store=False,
        )

    def only_online_stores(self):
        return self.filter(
            is_online_store=True,
        )

    def related(self):
        return self.prefetch_related(
            Prefetch(
                'addresses',
                queryset=Address.objects.active()
            ),
            Prefetch(
                'images',
                queryset=Image.objects.active(),
            ),
            'schedules',
        )
