# base
from base.managers import QuerySet

# geo django
from django.contrib.gis.db.models.functions import Distance
from django.contrib.gis.measure import D


class AddressQuerySet(QuerySet):
    def active(self):
        return self.filter(is_active=True)

    def active_stores(self):
        return self.filter(store__is_active=True)

    def only_physical_stores(self):
        return self.filter(store__is_online_store=False)

    def search_by_location(self, current_location, meters=5000):
        return (
            self.filter(
                location__distance_lte=(
                    current_location,
                    D(m=meters),
                ),
            )
            .annotate(
                distance=Distance(
                    "location",
                    current_location,
                )
            )
            .order_by("distance")
        )

    def related(self):
        return self.select_related("store")
