# base
from base.managers import QuerySet


class ImageQuerySet(QuerySet):

    def active(self):
        return self.filter(
            is_active=True,
        )
