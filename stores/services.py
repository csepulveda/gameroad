# gmaps
import googlemaps

# django
from django.conf import settings

# geodjango
from django.contrib.gis.geos import Point


def get_geo_point_from_address(address: str) -> Point:
    gmaps = googlemaps.Client(key=settings.GOOGLE_MAP_API_KEY)
    geocode_result = gmaps.geocode(address)

    if len(geocode_result) == 0:
        return None

    location = geocode_result[0].get('geometry', {}).get('location', {})

    return Point(
        location.get('lng', 0),
        location.get('lat', 0),
    )
