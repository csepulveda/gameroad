# -*- coding: utf-8 -*-
""" Forms for the stores application. """
# standard library

# django
from django import forms

# models
from stores.models import DraftStore
from stores.models import OnlineStore

# views
from base.forms import BaseModelForm

# recaptcha
from captcha.fields import ReCaptchaField
from captcha.widgets import ReCaptchaV3


class OnlineStoreForm(BaseModelForm):

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.fields['website'].required = True

    class Meta:
        model = OnlineStore
        fields = (
            'name',
            'website',
            'facebook',
            'instagram',
            'is_active',
        )


class DraftStoreForm(BaseModelForm):
    social_fields = (
        'website',
        'facebook',
        'instagram',
    )

    captcha = ReCaptchaField(
        label='',
        widget=ReCaptchaV3,
    )

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.fields['is_online_store'].label = '¿Es una tienda solamente online?'

        for field in self.fields:
            self.fields[field].widget.attrs['placeholder'] = ''

        self.fields['facebook'].help_text = (
            'Coloca acá la URL de facebook'
        )
        self.fields['instagram'].help_text = (
            'Coloca acá la URL de instagram'
        )
        self.fields['website'].help_text = (
            'Coloca acá la URL de la tienda'
        )
        self.fields['address'].help_text = (
            'Si no es una tienda online, coloca la dirección de la tienda acá'
        )

    class Meta:
        model = DraftStore
        fields = (
            'name',
            'is_online_store',
            'address',
            'website',
            'facebook',
            'instagram',
            'captcha',
        )

    def clean(self):
        cleaned_data = super().clean()

        is_online_store = cleaned_data['is_online_store']
        address = cleaned_data['address']

        if not address and not is_online_store:
            self.add_error(
                'address',
                'La tienda por lo que señalaste no es solamente online. Es necesario una dirección.',
            )

        for social_field in self.social_fields:
            if cleaned_data.get(social_field, ''):
                return

        msg = 'Tienes que rellenar por lo menos uno de estos campos para la tienda.'
        [self.add_error(sf, msg) for sf in self.social_fields]
        raise forms.ValidationError(
            'Tienes que rellenar por lo menos 1 de los siguientes campos: Website, Facebook, Instagram.'
        )
