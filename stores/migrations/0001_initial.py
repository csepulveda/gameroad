# Generated by Django 2.2.9 on 2020-02-08 17:27

import base.mixins
import base.models
import django.contrib.gis.db.models.fields
from django.db import migrations, models
import django.db.models.deletion
import easy_thumbnails.fields


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Store',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('created_at', models.DateTimeField(auto_now_add=True, help_text='creation date', verbose_name='created at')),
                ('updated_at', models.DateTimeField(auto_now=True, help_text='edition date', null=True, verbose_name='updated at')),
                ('name', models.CharField(max_length=255, verbose_name='name')),
                ('website', models.URLField(blank=True, max_length=150, verbose_name='website')),
                ('facebook', models.URLField(blank=True, max_length=150, verbose_name='facebook')),
                ('instagram', models.URLField(blank=True, max_length=150, verbose_name='instagram')),
                ('is_active', models.BooleanField(default=True, verbose_name='is active')),
                ('slug', models.SlugField(blank=True, editable=False, null=True)),
            ],
            options={
                'verbose_name': 'store',
                'verbose_name_plural': 'stores',
            },
            bases=(base.models.BaseModelMixin, base.mixins.AuditMixin, models.Model),
        ),
        migrations.CreateModel(
            name='Image',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('created_at', models.DateTimeField(auto_now_add=True, help_text='creation date', verbose_name='created at')),
                ('updated_at', models.DateTimeField(auto_now=True, help_text='edition date', null=True, verbose_name='updated at')),
                ('display_order', models.PositiveSmallIntegerField(default=0, verbose_name='display order')),
                ('image', easy_thumbnails.fields.ThumbnailerImageField(upload_to=base.models.file_path, verbose_name='image')),
                ('is_active', models.BooleanField(default=True, verbose_name='is active')),
                ('store', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='images', to='stores.Store', verbose_name='store')),
            ],
            options={
                'ordering': ('display_order',),
                'abstract': False,
            },
            bases=(base.models.BaseModelMixin, base.mixins.AuditMixin, models.Model),
        ),
        migrations.CreateModel(
            name='Address',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('created_at', models.DateTimeField(auto_now_add=True, help_text='creation date', verbose_name='created at')),
                ('updated_at', models.DateTimeField(auto_now=True, help_text='edition date', null=True, verbose_name='updated at')),
                ('address', models.CharField(max_length=255, verbose_name='address')),
                ('location', django.contrib.gis.db.models.fields.PointField(null=True, srid=4326)),
                ('is_active', models.BooleanField(default=True, verbose_name='is active')),
                ('store', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='addresses', to='stores.Store', verbose_name='store')),
            ],
            options={
                'abstract': False,
            },
            bases=(base.models.BaseModelMixin, base.mixins.AuditMixin, models.Model),
        ),
        migrations.CreateModel(
            name='Schedule',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('created_at', models.DateTimeField(auto_now_add=True, help_text='creation date', verbose_name='created at')),
                ('updated_at', models.DateTimeField(auto_now=True, help_text='edition date', null=True, verbose_name='updated at')),
                ('weekday', models.PositiveSmallIntegerField(choices=[(1, 'monday'), (2, 'tuesday'), (3, 'wednesday'), (4, 'thursday'), (5, 'friday'), (6, 'saturday'), (7, 'sunday')], verbose_name='weekday')),
                ('opening_time', models.TimeField(verbose_name='opening time')),
                ('closing_time', models.TimeField(verbose_name='closing time')),
                ('store', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='schedules', to='stores.Store', verbose_name='store')),
            ],
            options={
                'ordering': ('weekday',),
                'unique_together': {('store', 'weekday')},
            },
            bases=(base.models.BaseModelMixin, base.mixins.AuditMixin, models.Model),
        ),
    ]
