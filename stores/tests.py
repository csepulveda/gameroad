from django.urls import reverse
from unittest.mock import patch

from captcha.client import RecaptchaResponse

from base.tests import BaseTestCase

from stores.models import DraftStore

from stores.forms import DraftStoreForm


class StoreTestCase(BaseTestCase):

    def setUp(self):
        self.url = reverse('draft_store_create')

    @patch('captcha.fields.client.submit')
    def test_create_store_post_physical_store(self, mocked_submit):
        self.assertEqual(
            DraftStore.objects.count(),
            0
        )

        mocked_submit.return_value = RecaptchaResponse(is_valid=True)

        previous_data = {
            'name': 'foo',
            'address': 'bar',
            'website': 'example.com',
            'is_online_store': False,
        }

        new_data = {
            'name': 'foo',
            'website': 'example.com',
            'is_online_store': False,
        }

        previous_response = self.client.post(self.url, previous_data)
        new_response = self.client.post(self.url, new_data)

        self.assertRedirects(previous_response, self.url)
        self.assertEqual(new_response.status_code, 200)

        self.assertEqual(
            DraftStore.objects.count(),
            1
        )
