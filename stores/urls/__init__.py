from django.urls import path

from stores import views


urlpatterns = [
    path(
        '',
        views.AddressSearchListView.as_view(),
        name='address_search_list',
    ),
]
