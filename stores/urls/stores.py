from django.urls import path

from stores import views


urlpatterns = [
    path(
        'online/',
        views.StoreOnlineListView.as_view(),
        name='store_online_list',
    ),
    path(
        'create/',
        views.DraftStoreCreateView.as_view(),
        name='draft_store_create',

    ),
]
