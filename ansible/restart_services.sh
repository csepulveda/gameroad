#!/bin/bash
set -e
cd "$(dirname "$0")"

if [[ -n $1 ]]; then
  limit="$1"
else
  limit="remote"
fi

ansible-playbook --inventory inventory.yaml --limit "$limit" playbooks/restart-services.yaml

