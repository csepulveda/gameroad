# django
from django.conf.urls import include
from django.conf.urls import url

# rest framework
from rest_framework.routers import DefaultRouter

# views
from api.viewsets.address import AddressViewSet


# routers
router = DefaultRouter()

# regions
router.register(r'addresses', AddressViewSet, 'address')


app_name = 'api'
urlpatterns = [
    url(r'^', include(router.urls)),
]
