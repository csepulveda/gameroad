# rest framework
from rest_framework import viewsets
from rest_framework import mixins
from rest_framework.response import Response


from api.mixins import CreateModelMixin
from api.mixins import UpdateModelMixin
from api.mixins import DestroyModelMixin


class ReadOnlyModelViewSet(mixins.RetrieveModelMixin,
                           mixins.ListModelMixin,
                           viewsets.GenericViewSet):
    """
    A viewset that provides default `list()` and `retrieve()` actions.
    """
    model = None
    queryset = None
    serializer_detail = None
    serializer_create = None
    serializer_update = None

    def get_queryset(self):
        if self.queryset is None:
            return self.model.objects.all()
        return self.queryset

    def get_serializer(self, *args, **kwargs):
        """
        Return the serializer instance that should be used for validating and
        deserializing input, and for serializing output.
        """
        serializer_class = self.get_serializer_class()
        kwargs['context'] = self.get_serializer_context()
        return serializer_class(*args, **kwargs)

    def get_serializer_class(self):
        """
        Return the class to use for the serializer.
        Defaults to using `self.serializer_class`.
        You may want to override this if you need to provide different
        serializations depending on the incoming request.
        (Eg. admins get full serialization, others get basic serialization)
        """

        assert self.serializer_class is not None, (
            "'%s' should either include a `serializer_class` attribute, "
            "or override the `get_serializer_class()` method."
            % self.__class__.__name__
        )

        action = self.action

        serializer_action_classes = {
            'list': self.serializer_class,
            'retrieve': self.serializer_detail,
            'create': self.serializer_create,
            'update': self.serializer_update,
            'partial_update': self.serializer_update,
        }

        return serializer_action_classes.get(
            action) or self.serializer_class

    def pagination_for_custom_view(
            self, queryset, serializer_class, *args, **kwargs):
        """
        Pagination only for custom methods on view
        """

        queryset = self.filter_queryset(queryset)
        context = {
            'request': self.request
        }

        context.update(kwargs)

        page = self.paginate_queryset(queryset)
        if page is not None:
            # add pagination to queryset
            serializer = serializer_class(
                page,
                many=True,
                context=context,
            )
            return self.get_paginated_response(serializer.data)

        # return data without pagination
        serializer = serializer_class(
            queryset,
            many=True,
            context=context,
        )
        return Response(serializer.data)


class ModelViewSet(
        ReadOnlyModelViewSet,
        CreateModelMixin,
        UpdateModelMixin,
        DestroyModelMixin):

    """
    A viewset that provides default
    `list()`, `retrieve()`, `create()`,
    `update()` and `destroy()` actions.
    """
