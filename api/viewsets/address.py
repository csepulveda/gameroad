# viewsets
from . import ReadOnlyModelViewSet

# models
from stores.models.addresses import Address

# serializers
from api.serializers.address import AddressSerializer

# utils
from stores import utils

# enums
from logs.enums import LocationKind


class AddressViewSet(ReadOnlyModelViewSet):
    model = Address
    serializer_class = AddressSerializer

    def get_queryset(self):
        queryset = super().get_queryset()
        queryset = queryset.active().related()

        location = self.request.GET.get("location", None)

        if not location:
            return queryset

        _, queryset = utils.get_current_location(
            location, queryset, LocationKind.MOBILE
        )

        return queryset.only_physical_stores()
