from rest_framework import serializers

from stores.models import Store


class StoreSerializer(serializers.ModelSerializer):

    class Meta:
        model = Store
        fields = (
            'id',
            'name',
            'website',
            'facebook',
            'instagram',
            'is_online_store',
        )
