# Django Rest Framework
from rest_framework import serializers

# django
from stores.models.addresses import Address

from api.serializers.store import StoreSerializer


class AddressSerializer(serializers.ModelSerializer):
    distance = serializers.SerializerMethodField()
    store = StoreSerializer()

    class Meta:
        model = Address
        fields = (
            'id',
            'store_id',
            'store_name',
            'address',
            'location',
            'is_active',
            'distance',
            'store',
            'get_map_url',
        )

    def get_distance(self, obj):
        if not hasattr(obj, 'distance'):
            return ''
        return '{:.2f} KMS'.format(obj.distance.km)
